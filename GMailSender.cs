﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace TDK.Tools
{
    public static class GMailSender
    {
        #region Public mehtods
        public static async Task TrySendAsync(MailAddress fromAddress, string password, MailAddress toAddress, string subject, string body, bool isHtml = false)
        {
            //var fromAddress = new MailAddress("from@gmail.com", "From Name");
            //var toAddress = new MailAddress("to@example.com", "To Name");
            //const string fromPassword = "fromPassword";
            //const string subject = "Subject";
            //const string body = "Body";

            try
            {
                using (var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, password)
                })
                {
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = isHtml
                    })
                    {
                        await smtp.SendMailAsync(message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error sending email in GMailSender.TrySendAsync.", ex);
            }
        }
        #endregion
    }
}
