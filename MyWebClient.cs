﻿using System;
using System.Net;

namespace TDK.Tools
{
    public class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri address)
        {
            var w = base.GetWebRequest(address);
            w.Timeout = 20 * 60 * 1000;
            return w;
        }
    }
}
