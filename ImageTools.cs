﻿using ExifLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text;
using System.Linq;
using MimeTypeMap;

namespace TDK.Tools
{
    public static class ImageTools
    {
        #region Public methods

        public static string GetMimeTypeFromExtension(string extension)
        {
            var l = MimeTypeMap.List.MimeTypeMap.GetMimeType(extension).FirstOrDefault();
            return l == null ? string.Empty : l;
        }

        public static Stream ToStream(this Image image, ImageFormat format)
        {
            var stream = new System.IO.MemoryStream();
            image.Save(stream, format);
            stream.Position = 0;
            return stream;
        }

        public static DateTime GetImageDateTime(Image img)
        {
            var dateCreated = DateTime.Now;
            try
            {
                // GDI+ provides standardised definitions!
                // http://msdn.microsoft.com/en-us/library/ms534415(VS.85).aspx
                var propItem = img.GetPropertyItem(0x132);
                if (propItem != null)
                {
                    var encoding = new ASCIIEncoding();
                    var text = encoding.GetString(propItem.Value, 0, propItem.Len - 1);

                    var provider = CultureInfo.InvariantCulture;
                    dateCreated = DateTime.ParseExact(text, "yyyy:MM:d H:m:s", provider);
                }
                else
                {
                    throw new ArgumentException("Property Not Found");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error getting creation date", ex);
            }
            return dateCreated;
        }

        public static ImageInfo GetMetaDataExif(Stream imageStream)
        {
            var jpegInfo = ExifReader.ReadJpeg(imageStream);
            return new ImageInfo
            {
                Artist = jpegInfo.Artist,
                Copyright = jpegInfo.Copyright,
                DateTime = jpegInfo.DateTime,
                DateTimeOriginal = jpegInfo.DateTimeOriginal,
                Description = jpegInfo.Description,
                ExposureTime = jpegInfo.ExposureTime,
                FileName = jpegInfo.FileName,
                FileSize = jpegInfo.FileSize,
                Flash = (ImageInfo.FlashTypes)jpegInfo.Flash,
                FNumber = jpegInfo.FNumber,
                GpsLatitude = jpegInfo.GpsLatitude,
                GpsLatitudeRef = (ImageInfo.GpsLatitudeRefs)jpegInfo.GpsLatitudeRef,
                GpsLongitude = jpegInfo.GpsLongitude,
                GpsLongitudeRef = (ImageInfo.GpsLongitudeRefs)jpegInfo.GpsLongitudeRef,
                Height = jpegInfo.Height,
                IsColor = jpegInfo.IsColor,
                IsValid = jpegInfo.IsValid,
                LoadTime = jpegInfo.LoadTime,
                Make = jpegInfo.Make,
                Model = jpegInfo.Model,
                Orientation = (ImageInfo.Orientations)jpegInfo.Orientation,
                ResolutionUnit = (ImageInfo.ResolutionUnits)jpegInfo.ResolutionUnit,
                Software = jpegInfo.Software,
                ThumbnailData = jpegInfo.ThumbnailData,
                ThumbnailOffset = jpegInfo.ThumbnailOffset,
                ThumbnailSize = jpegInfo.ThumbnailSize,
                UserComment = jpegInfo.UserComment,
                Width = jpegInfo.Width,
                XResolution = jpegInfo.XResolution,
                YResolution = jpegInfo.YResolution
            };
        }

        public static void GetMetaData(Image img)
        {
            var tempProps = new List<string>();
            var propItems = img.PropertyItems;
            var count = 0;
            foreach (PropertyItem item in propItems)
            {
                tempProps.Add("Property Item " + count.ToString());
                tempProps.Add("iD: 0x" + item.Id.ToString("x"));
                count++;
            }

            var encodings = new ASCIIEncoding();
            try
            {
                var make = encodings.GetString(propItems[1].Value);
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting metadata from image.", ex);
            }
        }

        public static Image SetCopyrightAndArtist(Image image, string copyright, string artist)
        {
            var prop = image.PropertyItems[0];
            SetProperty(ref prop, 33432, copyright);
            image.SetPropertyItem(prop);

            prop = image.PropertyItems[0];
            SetProperty(ref prop, 315, artist);
            image.SetPropertyItem(prop);

            return image;
        }



        public static Image FixedSize(Image imgPhoto, int Width, int Height, Color fillColor, bool transparent)
        {
            var sourceWidth = imgPhoto.Width;
            var sourceHeight = imgPhoto.Height;
            const int sourceX = 0;
            const int sourceY = 0;
            var destX = 0;
            var destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((Width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((Height -
                              (sourceHeight * nPercent)) / 2);
            }

            var destWidth = (int)(sourceWidth * nPercent);
            var destHeight = (int)(sourceHeight * nPercent);

            var bmPhoto = new Bitmap(Width, Height,
                              PixelFormat.Format32bppArgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                             imgPhoto.VerticalResolution);

            var grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(fillColor);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            if (transparent)
            {
                bmPhoto.MakeTransparent(fillColor);
            }
            return bmPhoto;
        }

        public static Image AddTextBlock(Image img, Color fillColor, string text, bool border = true)
        {
            // Load the image and determine new dimensions
            const int textHeight = 20;

            var szDimensions = new Size(img.Width, img.Height + textHeight);

            // Create blank canvas
            var resizedImg = new Bitmap(szDimensions.Width, szDimensions.Height);
            using (Graphics gfx = Graphics.FromImage(resizedImg))
            {
                var rectf = new RectangleF(0, img.Height, img.Width, textHeight);

                gfx.Clear(fillColor);
                gfx.InterpolationMode =
                        InterpolationMode.HighQualityBicubic;

                gfx.DrawImage(img, 0, 0, img.Width, img.Height);

                if (border)
                {
                    using (var pen = new Pen(Color.Black, 2))
                    {
                        gfx.DrawRectangle(pen, 0, 0, szDimensions.Width, szDimensions.Height);
                    }
                }
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                var sf = new StringFormat
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };
                using (var font = new Font("Tahoma", 12, FontStyle.Regular))
                {
                    gfx.DrawString(text, font, Brushes.Black, rectf, sf);
                }
            }


            return resizedImg;
        }

        public static byte[] ImageToByteArray(Image image)
        {
            var _imageConverter = new ImageConverter();
            var imageBytes = (byte[])_imageConverter.ConvertTo(image, typeof(byte[]));
            return imageBytes;
        }

        /// <summary>
        /// Method that uses the ImageConverter object in .Net Framework to convert a byte array,
        /// presumably containing a JPEG or PNG file image, into a Bitmap object, which can also be
        /// used as an Image object.
        /// </summary>
        /// <param name="byteArray">byte array containing JPEG or PNG file image or similar</param>
        /// <returns>Bitmap object if it works, else exception is thrown</returns>
        public static Bitmap ImageFromByteArray(byte[] byteArray)
        {
            var _imageConverter = new ImageConverter();
            var bm = (Bitmap)_imageConverter.ConvertFrom(byteArray);

            if (bm != null && ((int)bm.HorizontalResolution != (int)bm.HorizontalResolution ||
                               (int)bm.VerticalResolution != (int)bm.VerticalResolution))
            {
                // Correct a strange glitch that has been observed in the test program when converting
                //  from a PNG file image created by CopyImageToByteArray() - the dpi value "drifts"
                //  slightly away from the nominal integer value
                bm.SetResolution((int)(bm.HorizontalResolution + 0.5f),
                                 (int)(bm.VerticalResolution + 0.5f));
            }

            return bm;
        }

        /// <summary>
        /// Utillity method for createing a grayscale copy of image
        /// </summary>
        /// <param name="original">original image</param>
        /// <returns>grayscale copy of image</returns>
        public static Bitmap MakeGrayscale3(Image original)
        {
            var newBitmap = new Bitmap(original.Width, original.Height);

            using (var g = Graphics.FromImage(newBitmap))
            {

                // create the grayscale ColorMatrix
                var colorMatrix = new ColorMatrix(
                    new float[][]
                        {
                            new float[] {.3f, .3f, .3f, 0, 0},
                            new float[] {.59f, .59f, .59f, 0, 0},
                            new float[] {.11f, .11f, .11f, 0, 0},
                            new float[] {0, 0, 0, 1, 0},
                            new float[] {0, 0, 0, 0, 1}
                        });

                // create some image attributes
                var attributes = new ImageAttributes();

                // set the color matrix attribute
                attributes.SetColorMatrix(colorMatrix);

                // draw the original image on the new image
                // using the grayscale color matrix
                g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
                            0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);
                g.Dispose();
            }

            return newBitmap;
        }

        #endregion

        #region Private methods
        private static void SetProperty(ref PropertyItem prop, int iId, string sTxt)
        {
            var iLen = sTxt.Length + 1;
            var bTxt = new Byte[iLen];
            for (int i = 0; i < iLen - 1; i++)
                bTxt[i] = (byte)sTxt[i];
            bTxt[iLen - 1] = 0x00;
            prop.Id = iId;
            prop.Type = 2;
            prop.Value = bTxt;
            prop.Len = iLen;
        }
        #endregion
    }
}
