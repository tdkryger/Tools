﻿using System.Collections.Generic;

namespace TDK.Tools
{
    public static class HtmlTools
    {
        public static string HtmlTemplate(string html, Dictionary<string, string> values)
        {
            var returnHtml = html;

            foreach (KeyValuePair<string, string> entry in values)
            {
                returnHtml = returnHtml.Replace(entry.Key, entry.Value);
            }

            return returnHtml;
        }
    }
}
