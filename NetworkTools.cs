﻿// Project: TDK.Tools
// (c)  2016 Thomas Kryger
// NetworkTools.cs

using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace TDK.Tools
{
    public static class NetworkTools
    {
        /// <summary>
        /// Usage:
        /// Now to get the IPv4 address of your Ethernet network interface call:
        /// GetLocalIPv4(NetworkInterfaceType.Ethernet);
        /// Or your Wireless interface:
        /// GetLocalIPv4(NetworkInterfaceType.Wireless80211);
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetLocalIPv4(NetworkInterfaceType type)
        {
            var output = string.Empty;
            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (item.NetworkInterfaceType == type && item.OperationalStatus == OperationalStatus.Up)
                {
                    var adapterProperties = item.GetIPProperties();

                    if (adapterProperties.GatewayAddresses.FirstOrDefault() != null)
                    {
                        foreach (UnicastIPAddressInformation ip in adapterProperties.UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                output = ip.Address.ToString();
                            }
                        }
                    }
                }
            }

            return output;
        }

        /// <summary>
        /// Check if an email address is valid
        /// </summary>
        /// <param name="email">The email to check</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool IsValidEMail(string email)
        {
            return new EmailAddressAttribute().IsValid(email);
        }

        /// <summary>
        /// Checks if a url is valid. Requires an internet connection
        /// </summary>
        /// <param name="url">The url to check</param>
        /// <returns>True if valid, false otherwise</returns>
        public static bool IsValidUrl(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                var request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                var response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
#pragma warning disable CC0003 // Your catch should include an Exception
            catch
            {
                //Any exception will returns false.
                return false;
            }
#pragma warning restore CC0003 // Your catch should include an Exception
        }
    }
}
