﻿// Project: TDK.Tools
// (c)  2016-2018 Thomas D. Kryger
// StringExtension.cs
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace TDK.Tools.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        ///   Remove digits from string.
        /// </summary>
        /// <param name="key">the string</param>
        /// <returns>A string without digits.</returns>
        public static string RemoveDigits(this string key)
        {
            return Regex.Replace(key, @"\d", "");
        }

        public static string RemoveUnwantedHtmlTags(this string html, List<string> unwantedTags = null)
        {
            if (unwantedTags == null)
                unwantedTags = new List<string>();
            if (String.IsNullOrEmpty(html))
            {
                return html;
            }

            var document = new HtmlDocument();
            document.LoadHtml(html);

            var tryGetNodes = document.DocumentNode.SelectNodes("./*|./text()");

            if (tryGetNodes == null || !tryGetNodes.Any())
            {
                return html;
            }

            var nodes = new Queue<HtmlNode>(tryGetNodes);

            while (nodes.Count > 0)
            {
                var node = nodes.Dequeue();
                var parentNode = node.ParentNode;

                var childNodes = node.SelectNodes("./*|./text()");

                if (childNodes != null)
                {
                    foreach (var child in childNodes)
                    {
                        nodes.Enqueue(child);
                    }
                }

                if (unwantedTags.Count == 0)
                {
                    parentNode.RemoveChild(node);
                }
                else
                {
                    if (unwantedTags.Any(tag => tag == node.Name))
                    {
                        if (childNodes != null)
                        {
                            foreach (var child in childNodes)
                            {
                                parentNode.InsertBefore(child, node);
                            }
                        }

                        parentNode.RemoveChild(node);
                    }
                }
            }

            return document.DocumentNode.InnerHtml;
        }

        public static bool IsValidEmail(this string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static string FirstLetterToUpper(this string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        public static void CheckForNullOrEmpty(this string value)
        {
            if (value == string.Empty)
                throw new ArgumentException($"string is empty");
            if (value == null)
                throw new ArgumentNullException($"string is null");
        }

        public static string Capitalize(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;
            else
                return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(text.ToLower());
        }
    }
}
