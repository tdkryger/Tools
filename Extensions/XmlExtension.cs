﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace TDK.Tools.Extensions
{
    public static class XmlExtension
    {
        #region Public mehtods
        public static IEnumerable<XElement> ElementsAnyNS<T>(this IEnumerable<T> source, string localName) where T : XContainer
        {
            return source.Elements().Where(e => e.Name.LocalName == localName);
        }
        #endregion
    }
}
