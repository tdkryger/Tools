﻿namespace TDK.Tools
{
    public static class IntTools
    {
        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }
    }
}
