﻿// Project: TDK.Tools
// (c)  2016 Thomas Kryger
// StringTools.cs

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace TDK.Tools
{
    public static class StringTools
    {
        private static readonly Random random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        ///
        /// </summary>
        /// <param name="inputString"></param>
        /// <param name="hashName">See https://msdn.microsoft.com/en-us/library/wet69s13.aspx </param>
        /// <returns></returns>
        public static string HashString(string inputString, string hashName)
        {
            var algorithm = HashAlgorithm.Create(hashName);
            if (algorithm == null)
            {
                throw new ArgumentException("Unrecognized hash name", nameof(hashName));
            }
            var hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
            return Convert.ToBase64String(hash);
        }

        public static string RandomString(int length = 5)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            var data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            foreach (byte t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            var hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            var comparer = StringComparer.OrdinalIgnoreCase;

            return 0 == comparer.Compare(hashOfInput, hash);
        }

        public static decimal StringToDecimal(string text, decimal defaultValue = 0)
        {
            if (string.IsNullOrEmpty(text))
                return defaultValue;
            return decimal.TryParse(text, out decimal dec) ? dec : defaultValue;
        }

        public static string Capitalize(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new ArgumentNullException(nameof(text), "StringTools.Capitalize: Text is null");
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(text.ToLower());
        }

        public static string ShortString(string text, int count)
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text), "StringTools.ShortString: Text is null");
            var temp = text;
            if (count < text.Length)
            {
                temp = text.Substring(0, count);
                var idx = temp.LastIndexOf(' ');
                temp = temp.Substring(0, idx).Trim() + "...";
            }

            return temp;
        }

        public static bool GetIntegerFromString(string input, out int value)
        {
            var stack = new Stack<char>();

            for (var i = input.Length - 1; i >= 0; i--)
            {
                if (!char.IsNumber(input[i]))
                {
                    break;
                }

                stack.Push(input[i]);
            }

            var result = new string(stack.ToArray());
            value = 0;
            return int.TryParse(result, out value);
        }

        public static string RemoveIlligalCharsInFilepath(string input)
        {
            var regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(input, "");
        }
    }
}
