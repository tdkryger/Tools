﻿using System;

namespace TDK.Tools
{
    public class ImageInfo
    {
        #region Enums
        public enum GpsLongitudeRefs { Unknown, East, West };
        public enum GpsLatitudeRefs { Unknown, North, South };
        public enum FlashTypes { No = 0, Fired = 1, StrobeReturnLightDetected = 6, On = 8, Off = 16, Auto = 24, FlashFunctionPresent = 32, RedEyeReduction = 64 }
        public enum ResolutionUnits { Undefined = 1, Inch = 2, Centimeter = 3 };
        public enum Orientations { TopLeft = 1, BottomRight = 3, TopRight = 6, BottomLeft = 8, Undefined = 9 };
        #endregion

        #region Properties
        public double[] GpsLatitude;
        public double[] GpsLongitude;

        public int ThumbnailSize { get; set; }
        public int ThumbnailOffset { get; set; }
        public GpsLongitudeRefs GpsLongitudeRef { get; set; }
        public GpsLatitudeRefs GpsLatitudeRef { get; set; }
        public FlashTypes Flash { get; set; }
        public ResolutionUnits ResolutionUnit { get; set; }
        public Orientations Orientation { get; set; }
        public double FNumber { get; set; }
        public double ExposureTime { get; set; }
        public string UserComment { get; set; }
        public string Copyright { get; set; }
        public string Artist { get; set; }
        public string Software { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public string Description { get; set; }
        public string DateTimeOriginal { get; set; }
        public string DateTime { get; set; }
        public double YResolution { get; set; }
        public double XResolution { get; set; }
        public bool IsColor { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsValid { get; set; }
        public int FileSize { get; set; }
        public string FileName { get; set; }
        public byte[] ThumbnailData { get; set; }
        public TimeSpan LoadTime { get; set; }
        #endregion
    }
}
