﻿using System.Xml;

namespace TDK.Tools
{
    public static class XmlTools
    {
        public static string GetElementValue(XmlNode node, string tagName)
        {
            var value = string.Empty;
            var xn = node.SelectSingleNode(tagName);
            if (xn != null)
            {
                value = xn.InnerText;
            }
            return value;
        }

        public static string GetAttributeValue(XmlNode node, string attName)
        {
            var value = string.Empty;
            if (node != null)
            {
                foreach (XmlAttribute att in node.Attributes)
                {
                    if (att.Name == attName)
                        value = att.InnerText;
                }
            }
            return value;
        }

        public static string GetElementValue(XmlNode node, string tagName, string attName)
        {
            var value = string.Empty;
            var xn = node.SelectSingleNode(tagName);
            if (xn == null)
                xn = node;
            if (xn != null)
            {
                foreach (XmlAttribute att in xn.Attributes)
                {
                    if (att.Name == attName)
                        value = att.InnerText;
                }
            }
            return value;
        }

        public static string GetElementValue(string xml, string tagName, string attValue, string attName)
        {
            var doc = new XmlDocument();
            try
            {
                doc.LoadXml(xml);
                var fbid = doc.GetElementsByTagName(tagName);
                foreach (XmlNode node in fbid)
                {
                    foreach (XmlAttribute att in node.Attributes)
                    {
                        if (att.Name == attName && att.Value == attValue)
                            return node.InnerText;
                    }
                }
            }
            catch
            { // Do something..
            }
            return string.Empty;
        }

        public static string GetElementValue(string xml, string tagName, int index = 0)
        {
            var doc = new XmlDocument();
            try
            {
                doc.LoadXml(xml);
                var fbid = doc.GetElementsByTagName(tagName);
                if (fbid.Count > index)
                {
                    return fbid[index].InnerText;
                }
            }
            catch
            { // Do something..
            }

            return string.Empty;
        }

        public static string GetElementValue(XmlDocument _xmlDoc, string tagName, int index = 0)
        {
            var fbid = _xmlDoc.GetElementsByTagName(tagName);
            if (fbid.Count > index)
            {
                return fbid[index].InnerText;
            }
            return string.Empty;
        }
    }
}
