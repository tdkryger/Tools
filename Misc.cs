﻿// Project: TDK.Tools
// (c)  2016 Thomas Kryger
// Misc.cs

using System.Diagnostics;
using System.Reflection;

namespace TDK.Tools
{
    public static class Misc
    {
        public static bool IsRelease(Assembly assembly)
        {
            var attributes = assembly.GetCustomAttributes(typeof(DebuggableAttribute), true);
            if (attributes.Length == 0)
                return true;

            var d = (DebuggableAttribute)attributes[0];
            if ((d.DebuggingFlags & DebuggableAttribute.DebuggingModes.Default) == DebuggableAttribute.DebuggingModes.None)
                return true;

            return false;
        }

        public static bool IsDebug(Assembly assembly)
        {
           var attributes = assembly.GetCustomAttributes(typeof(DebuggableAttribute), true);
            if (attributes.Length == 0)
                return true;

            var d = (DebuggableAttribute)attributes[0];
            if (d.IsJITTrackingEnabled) return true;
            return false;
        }
    }
}
